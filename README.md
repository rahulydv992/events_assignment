# events_assignments



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.


## Backend

The backend use standard Express starter kit along with
* Joi for validation
* Mongoose for MongoDB

to run the code, goto backend folder and do
* yarn install
* yarn start

## Frontend

The frontend use standard react starter kit with
* Ant Design
* Redux
* Thunk

to run the code, goto frontend and do 
* yarn install
* yarn start