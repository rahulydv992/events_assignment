export const filterQueryParams = (obj) => {
  let filterObj = {};
  for (let key in obj) {
    if (obj[key] || (key === "isVirtual" && obj[key] !== undefined)) {
      filterObj[key] = obj[key];
    }
  }
  return filterObj;
};
