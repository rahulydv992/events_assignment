import {
  FETCH_EVENTS,
  SET_CATEGORIES,
  SET_FILTER_OBJ,
  UPDATE_PAGE_NUM,
} from "./ActionTypes";

const initState = {
  events: [],
  filterObj: {},
  total: 1,
  categories: [],
  page: 1,
};

export const eventsReducer = (state = initState, { type, payload }) => {
  console.log({type, payload})
  switch (type) {
    case FETCH_EVENTS:
      return {
        ...state,
        ...payload
      };
    case UPDATE_PAGE_NUM:
      return { ...state, ...payload };
    case SET_CATEGORIES:
      return { ...state, ...payload };
    case SET_FILTER_OBJ:
      return { ...state, filterObj: payload, page:1 };
    default:
      return state;
  }
};
