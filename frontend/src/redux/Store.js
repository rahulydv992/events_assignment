import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { eventsReducer } from "./Reducer";

export const Store = createStore(eventsReducer, applyMiddleware(thunk));