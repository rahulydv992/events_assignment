import {
  FETCH_EVENTS,
  SET_CATEGORIES,
  SET_FILTER_OBJ,
  UPDATE_PAGE_NUM,
} from "./ActionTypes";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {filterQueryParams} from "../utils/helper";

const apiUrl = process.env.REACT_APP_API_URL;

export const fetchEvents = (filterParams) => (dispatch) => {
  const { page, ...rest } = filterParams;
  const obj = filterQueryParams(rest);
  const params = new URLSearchParams(obj).toString();
  axios
    .get(`${apiUrl}?page=${page}&${params}`)
    .then(({ data }) => {
      dispatch(({ type: FETCH_EVENTS, payload: data }))
    })
    .catch((err) => {
      toast.error(err.response.data.message);
    });
};

export const fetchCategories = () => (dispatch) => {
  axios
    .get(`${apiUrl}/categories`)
    .then(({ data }) => {
      dispatch({ type: SET_CATEGORIES, payload: {categories: data} });
    })
    .catch((err) => {
      toast.error(err.response.data.message);
    });
};

export const updatePageNum = (page) => ({
  type: UPDATE_PAGE_NUM,
  payload: {page},
});

export const setFilterObj = (data) => ({
  type: SET_FILTER_OBJ,
  payload: data,
});


