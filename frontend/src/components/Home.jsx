import {EventCart} from "./EventCart";
import {Alert, Pagination} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories, fetchEvents, updatePageNum} from "../redux/Actions";
import {Filter} from "./Filter";
import {useEffect} from "react";

export const Home = () => {
    const {events, total, page, filterObj} = useSelector((store) => store);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCategories());
    }, []);

    useEffect(() => {
        dispatch(fetchEvents({...filterObj, page}));
    }, [page, filterObj]);

    return (
        <div className={"App"}>
            <Filter/>
            <div className="cart-container">
                {!events.length ? (
                    <div style={{gridColumn: "1 / span 2"}}>
                        <Alert
                            message="Sorry! there is no events available"
                            type="warning"
                            style={{width: "40%", margin: "auto"}}
                        />
                    </div>
                ) : (
                    <>
                        {events.map((event) => {
                            return <EventCart key={event._id} event={event}/>;
                        })}
                        <div style={{gridColumn: "1 / span 2", margin: "20px", textAlign: "center"}}>
                            <Pagination
                                total={total}
                                current={+page}
                                pageSize={4}
                                onChange={(e) => {
                                    dispatch(updatePageNum(e));
                                }}
                            />
                        </div>
                    </>
                )}
            </div>
        </div>
    );
};
