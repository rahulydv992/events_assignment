import { Button, Input, Select } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchEvents, setFilterObj, updatePageNum } from "../redux/Actions";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const { Option } = Select;

export const Filter = () => {

  const { categories, page, filterObj } = useSelector((store) => store);
    const [{ title, address, isVirtual, category }, setFilter] =
        useState({});
  const dispatch = useDispatch();

  useEffect(() => {
      setFilter(filterObj);
  }, [filterObj]);

  return (
    <div className="filter-container">
      <Input
        size="large"
        placeholder="Filter by Title"
        name="title"
        value={title}
        onChange={(e) => {
          setFilter((prev) => {
            return { ...prev, title: e.target.value };
          });
        }}
      />
      <Input
        size="large"
        placeholder="Filter by address"
        name="address"
        value={address}
        onChange={(e) => {
          setFilter((prev) => {
            return { ...prev, address: e.target.value };
          });
        }}
      />
      <Select
        style={{
          width: 520,
        }}
        size="large"
        placeholder="Filter by mode"
        name="isVirtual"
        value={isVirtual}
        onChange={(value) => {
          setFilter((prev) => {
            return { ...prev, isVirtual: value };
          });
        }}
      >
        <Option value={true}>Virtual</Option>
        <Option value={false}>Physical</Option>
      </Select>
      <Select
        style={{
          width: 520,
        }}
        size="large"
        placeholder="Filter by category"
        value={category}
        onChange={(value) => {
          setFilter((prev) => {
            return { ...prev, category: value };
          });
        }}
      >
        {categories.map((categoryObj) => {
          return (
            <Option value={categoryObj.category} key={categoryObj._id}>
              {categoryObj.category}
            </Option>
          );
        })}
      </Select>
      <Button
        type="primary"
        size="large"
        onClick={() => {
          if (!title && !address && isVirtual === null && !category) {
            return toast.error("all fields can't be empty");
          }
          dispatch(setFilterObj({ title, address, isVirtual, category }));
        }}
      >
        Filter
      </Button>
      <Button
        type="primary"
        size="large"
        onClick={() => {
          setFilter({});
          dispatch(setFilterObj({}));
        }}
      >
        Reset
      </Button>
    </div>
  );
};
