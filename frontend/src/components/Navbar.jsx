import { Outlet, useNavigate } from "react-router-dom";

export const Navbar = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className="navbar">
        <button
          onClick={() => {
            navigate("/create");
          }}
        >
          Create Event
        </button>
        <button
          onClick={() => {
            navigate("/");
          }}
        >
          Home
        </button>
      </div>
      <Outlet />
    </>
  );
};
