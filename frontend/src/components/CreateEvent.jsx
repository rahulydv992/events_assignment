import {Button, Checkbox, DatePicker, Input} from "antd";
import {useState} from "react";
import axios from "axios";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {useNavigate} from "react-router-dom";

const apiUrl = process.env.REACT_APP_API_URL;
const apiKey = process.env.REACT_APP_API_KEY;

const initState = {
    title: "",
    description: "",
    category: "",
    date: "",
    isVirtual: false,
    address: "",
};

export const CreateEvent = () => {
    const [event, setEvent] = useState(initState);
    const navigate = useNavigate();

    const handleChange = (e) => {
        let {name, value, checked} = e.target;
        value = name === "isVirtual" ? checked : value;
        setEvent({...event, [name]: value});
    };

    const handleSubmit = async () => {
        try {
            await axios.post(apiUrl, event, {
                headers: {
                    'x-api-key':apiKey
                }
            });
            toast.success("Event created successfully");
            navigate("/");
        } catch (err) {
            toast.error(err.response.data.message);
        }
    };

    return (
        <div className="createCart">
            <Input
                placeholder="Enter title"
                value={event.title}
                onChange={handleChange}
                size="large"
                name="title"
            />
            <Input
                placeholder="Enter description"
                value={event.description}
                onChange={handleChange}
                size="large"
                name="description"
            />
            <Input
                placeholder="Enter category"
                value={event.category}
                onChange={handleChange}
                size="large"
                name="category"
            />
            <DatePicker
                size="large"
                onChange={(e, date) => {
                    setEvent({...event, date});
                }}
            />
            <Checkbox
                checked={event.isVirtual}
                onChange={handleChange}
                size="large"
                name="isVirtual"
            >
                is Virtual
            </Checkbox>
            <Input
                placeholder="Enter address"
                value={event.address}
                onChange={handleChange}
                size="large"
                name="address"
            />
            <Button type="primary" size="large" onClick={handleSubmit}>
                Create
            </Button>
        </div>
    );
};
