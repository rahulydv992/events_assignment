const mongoose = require("mongoose");
const { MONGO_DB_URL } = require("../utils/constants");

module.exports = () => {
  return mongoose
    .connect(MONGO_DB_URL)
    .then(() => {
      console.log("connection established");
    })
    .catch((err) => {
      console.log(err);
    });
};
