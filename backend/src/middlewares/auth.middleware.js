const authMiddleware = (req, res, next) => {
    const apiKey = req.headers['x-api-key'];
    if (apiKey === process.env.API_KEY)
        next();
    else return res.status(401).send({message: 'Invalid API key! Permission denied'})
}

module.exports = {authMiddleware}