const {Router} = require("express");
const Event = require("../models/event.model");
const {PAGE_LIMIT} = require("../utils/constants");
const {parseFilterObject} = require("../utils/helper");
const {EventSchema, FilterSchema} = require("./event.schema");
const {authMiddleware} = require("../middlewares/auth.middleware");
const router = Router();

router.post("", authMiddleware, async (req, res) => {
    try {
        const {value, error} = EventSchema.validate(req.body);
        if (error) return res.status(400).send({message: error.message});
        const event = await Event.create(value);
        return res.status(201).send(event);
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

router.get("", async (req, res) => {
    try {
        const {value, error} = FilterSchema.validate(req.query);
        if (error) return res.status(400).send({message: error.message});
        const {page, ...rest} = value;
        const findObj = parseFilterObject(rest);
        const skip = (page - 1) * PAGE_LIMIT;
        const events = await Event.find(findObj)
            .limit(PAGE_LIMIT)
            .skip(skip)
            .lean()
            .exec();
        const count = await Event.countDocuments(findObj);
        return res.status(200).send({
            events,
            total: count,
        });
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

router.get("/categories", async (req, res) => {
    try {
        const categories = await Event.find({}, {category: 1}).lean().exec();
        return res.status(200).send(categories);
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

router.get("/:id", async (req, res) => {
    try {
        const event = await Event.findById(req.params.id).lean().exec();
        return res.status(200).send(event);
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

router.patch("/:id", authMiddleware,async (req, res) => {
    try {
        const event = await Event.findByIdAndRemove(req.params.id, req.body, {
            new: true,
        })
            .lean()
            .exec();
        return res.status(201).send(event);
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

router.delete("/:id", authMiddleware,async (req, res) => {
    try {
        const event = await Event.findByIdAndDelete(req.params.id).lean().exec();
        return res.status(200).send(event);
    } catch (err) {
        return res.status(500).send({message: err.message});
    }
});

module.exports = router;
