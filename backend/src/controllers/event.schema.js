const joi = require("joi");

const EventSchema = joi.object({
    title: joi.string().required().min(8),
    description: joi
        .string()
        .min(12)
        .required(),
    category: joi
        .string()
        .min(8)
        .required(),
    date: joi.date().min("now").required(),
    isVirtual: joi
        .boolean()
        .required(),
    address: joi
        .string()
        .empty()
        .min(8)
        .required(),
});

const FilterSchema = joi.object({
    page: joi
        .number().integer().min(1).default(1),
    title: joi.string(),
    category: joi.string(),
    isVirtual: joi
        .boolean(),
    address: joi.string(),
});

module.exports = { EventSchema, FilterSchema };
