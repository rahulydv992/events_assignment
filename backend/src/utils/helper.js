const parseFilterObject = (obj) => {
    return Object.keys(obj).reduce((filterObj, key) => {
        if (key === "isVirtual") filterObj[key] = obj[key]
        else filterObj[key] = new RegExp(obj[key], "i")
        return filterObj;
    }, {})
}


module.exports = {parseFilterObject};
