const app = require("./index");
const connection = require("./config/db");
const {PORT} = require("./utils/constants");

const port = PORT || 2001;

connection().then(() => {
    app.listen(port, () => {
        console.log("Started listening on port " + port)
    });
}).catch((err) => {
    console.log("Error Establishing connection with MongoDB: " + err.message);
})
